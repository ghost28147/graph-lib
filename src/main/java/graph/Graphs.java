package graph;

public class Graphs {
    private Graphs() {}

    public static <V> Graph<V> directed() {
        return new DirectedGraph<>();
    }

    public static <V> Graph<V> undirected() {
        return new UndirectedGraph<>();
    }
}

