package graph;

import java.util.Objects;

class Edge<V> {
    Vertex<V> to;
    int weight;

    Edge(V to) {
        this.to = new Vertex<>(to);
        weight = 0;
    }

    Edge(Vertex<V> to) {
        this.to = to;
        weight = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge<?> edge = (Edge<?>) o;
        return to.equals(edge.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(to);
    }
}
