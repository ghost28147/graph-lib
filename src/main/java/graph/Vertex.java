package graph;

import java.util.Objects;

class Vertex<V> {
    V data;

    Vertex<V> parent;
    boolean visited;

    Vertex(V data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex<?> vertex = (Vertex<?>) o;
        return data.equals(vertex.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }
}
