package graph;

import java.util.HashSet;

class DirectedGraph<V> extends Graph<V> {

    @Override
    public boolean addEdge(V from, V to) {

        Vertex<V>  fromVertex  =  toVertex(from);
        Vertex<V>  toVertex    =  toVertex(to);

        return adjacentVertices
                .computeIfAbsent(fromVertex, k -> new HashSet<>())
                .add(new Edge<>(toVertex));
    }
}
