package graph;

import java.util.*;

public abstract class Graph<V> {

    // Graph is implemented as adjacency list,
    // where each vertex mapped to the list of its adjacent vertices.
    Map<Vertex<V>, Set<Edge<V>>> adjacentVertices = new HashMap<>();

    private Queue<Vertex<V>> searchQueue = new ArrayDeque<>();

    /**
     * @param v non-null vertex to add
     * @return true if graph did not already contain {@code v}
     *
     * @throws NullPointerException if added vertex is null
     */
    public boolean addVertex(V v) {
        if (v == null) throw new NullPointerException();

        Vertex<V> newVertex = new Vertex<>(v);
        return adjacentVertices.putIfAbsent(newVertex, new HashSet<>()) == null;
    }

    /**
     * Adds edge incident to given vertices.
     * Creates vertices in case graph doesn't contain ones
     * @return true if graph did not already contain given edge
     *
     */
    public abstract boolean addEdge(V from, V to);

    public boolean containsVertex(V vertex) {
        return adjacentVertices.containsKey(new Vertex<>(vertex));
    }

    public boolean containsEdge(V from, V to) {
        Set<Edge<V>> edges = adjacentVertices.get(new Vertex<>(from));

        return edges != null && edges.contains(new Edge<>(to));
    }

    /**
     * Returns the shortest path between given vertices.
     * @implNote uses breadth-first search (BFS) algorithm
     * to build search tree
     * */
    public List<V> getPath(V from, V to) {

        if (from.equals(to)) return Arrays.asList(from ,to);

        adjacentVertices.keySet().stream()
                .filter(vertex -> !vertex.data.equals(from))
                .forEach(this::reset);

        Vertex<V> source = adjacentVertices.keySet().stream()
                .filter(vertex -> vertex.data.equals(from))
                .findFirst().get();

        source.visited = true;
        source.parent = null;

        searchQueue.add(source);

        while (searchQueue.size() > 0) {
            Vertex<V> current = searchQueue.remove();
            for (Edge<V> adjacentVertex : adjacentVertices.get(current)) {
                if (!adjacentVertex.to.visited) {
                    adjacentVertex.to.visited = true;
                    adjacentVertex.to.parent = current;
                    searchQueue.add(adjacentVertex.to);
                }
            }
        }

        List<V> cooked = new ArrayList<>();
        Vertex<V> toVertex = adjacentVertices.keySet().stream()
                .filter(vertex -> vertex.data.equals(to)).findFirst().get();

        for (Vertex<V> current = toVertex; current != null; current = current.parent) {
            cooked.add(current.data);
        }

        Collections.reverse(cooked);

        return cooked;
    }

    Vertex<V> toVertex(V from) {
        return adjacentVertices.keySet().stream()
                .filter(v -> v.data.equals(from))
                .findFirst().orElse(new Vertex<>(from));
    }

    private void reset(Vertex<V> vertex) {
        vertex.parent = null;
        vertex.visited = false;
    }
}
