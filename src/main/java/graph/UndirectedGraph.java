package graph;

import java.util.HashSet;

class UndirectedGraph<V> extends Graph<V> {

    /**
     * @throws IllegalArgumentException if {@code from} and {@code to} vertices are the same
     * (i.e. attempting to add loop to graph)
     */
    @Override
    public boolean addEdge(V from, V to) {

        if (from.equals(to)) throw new IllegalArgumentException();

        Vertex<V> fromVertex = toVertex(from);
        Vertex<V> toVertex = toVertex(to);

        boolean isNewEdge = adjacentVertices
                .computeIfAbsent(fromVertex, k -> new HashSet<>())
                .add(new Edge<>(toVertex));

        return isNewEdge && adjacentVertices
                .computeIfAbsent(toVertex, k -> new HashSet<>())
                .add(new Edge<>(fromVertex));
    }

}
