package graph;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class UndirectedGraphTest {

    @Test(expected = NullPointerException.class)
    public void addVertex__Add_null_vertex__Throws_NPE() {
        Graph<?> g = Graphs.undirected();

        g.addVertex(null);
    }

    @Test
    public void addVertex__Attempt_to_add_already_existing_one__Returns_false() {
        Graph<Point> g = Graphs.undirected();

        g.addVertex(new Point(1, 1));
        boolean actual = g.addVertex(new Point(1, 1));

        assertThat(actual, is(false));
    }

    @Test
    public void addVertex__Add_new_vertex__Returns_true() {
        Graph<Integer> g = Graphs.undirected();

        boolean actual = g.addVertex(42);

        assertThat(actual, is(true));
    }

    @Test
    public void addEdge__Add_new_edge_from_existing_vertices__Edge_is_added() {
        Graph<Integer> g = Graphs.undirected();

        int fromVertex = 42;
        int toVertex = 0xCAFEBABE;
        g.addVertex(fromVertex);
        g.addVertex(toVertex);
        boolean isNewEdge = g.addEdge(fromVertex, toVertex);

        boolean expected = isNewEdge
                && g.containsEdge(fromVertex, toVertex)
                && g.containsEdge(toVertex, fromVertex);
        assertThat(expected, is(true));
    }

    @Test
    public void addEdge__Add_new_edge_while_vertices_are_not_in_graph__Edge_is_added() {

        Graph<Point> g = Graphs.undirected();

        Point fromVertex = new Point(0,0);
        Point toVertex = new Point(1,1);

        boolean isNewEdge = g.addEdge(fromVertex, toVertex);

        boolean expected = isNewEdge
                && g.containsEdge(fromVertex, toVertex)
                && g.containsEdge(toVertex, fromVertex)
                && g.containsVertex(fromVertex)
                && g.containsVertex(toVertex);

        assertThat(expected, is(true));
    }

    @Test
    public void addEdge__Add_existing_edge_Returns_false() {

        Graph<Point> g = Graphs.undirected();

        Point fromVertex = new Point(0,0);
        Point toVertex = new Point(1,1);

        g.addEdge(fromVertex, toVertex);
        boolean expected = g.addEdge(new Point(0, 0), new Point(1, 1));

        assertThat(expected, is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void addEdge__Attempt_to_add_loop__Throws_exception() {
        Graph<Point> g = Graphs.undirected();

        g.addEdge(new Point(1, 1), new Point(1, 1));
    }

    @Test
    public void getPath__Get_path_between_two_vertices__Returns_shortest_path() {
        Graph<Point> g = Graphs.undirected();

        g.addEdge(new Point(0, 0), new Point(1, 1));
        g.addEdge(new Point(1, 1), new Point(2, 2));
        g.addEdge(new Point(2, 2), new Point(3, 3));

        g.addEdge(new Point(0, 0), new Point(3, 0));
        g.addEdge(new Point(3, 0), new Point(3, 3));

        List<Point> actual = g.getPath(new Point(0, 0), new Point(3, 3));

        List<Point> expected = Arrays.asList(
                new Point(0, 0),
                new Point(3, 0),
                new Point(3, 3));

        assertThat(actual, equalTo(expected));
    }

    private static class Point {
        private final int x, y;

        Point(int x, int y) { this.x = x; this.y = y; }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return x == point.x &&
                    y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }
}